Terrain
=======
Looking at the scenery from up in the air is truly a magical experience, as there are too many details to observe but at the same time these details form the scenery.

Many games build this scenery by using a height map, some standard textures for different surfaces, and a mask to paint these textures on the surface. They add houses, trees and some other details. When done well, this is how it looks:

.. figure:: http://www.condorsoaring.com/wp-content/uploads/2018/02/ridge.jpg
   
   `Condor soaring simulator <condorsoaring.com>`_

There are also high-res satellite imagery add-ons for this simulator, but this makes rendering, storing and transmitting data for an online game very heavy.

Other games use a similar approach, like `GeoFS`_, which uses `CesiumJS`_ as rendering engine. `Ayvri`_ is another tool using Cesium, strictly designed for playback of routes. This all works quite well but the amount of data and rendering intensity of Cesium is very high, especially noticeable on less powerful devices or slow connections.

.. _GeoFS: https://geo-fs.com
.. _CesiumJS: https://cesiumjs.org
.. _Ayvri: https://ayvri.com/

In recent years the "low-poly" style has become popular, mainly for mobile games, which offers a different paradigm where no textures are used, and detail is formed by edges of the polygons.

TODO: Show example of low-poly game


Two flavors
-----------
We aim to provide two "flavors" of terrain:

- TIN-based with trees or buildings on top
- Block-based world with arbitrary detail

TIN terrain
-----------
This terrain type is made up of a tiled Triangular Irregular Network (TIN) terrain mesh. This is simply a Delaunay triangulation of the height map. Starting from just two triangles, at every step the point with the largest distance to the current TIN is added until a certain accuracy or number of points is reached. This algorithm is described in detail in [CompGeom]_, chapter 9.

There are a few GIS-oriented libraries that support generating this type of mesh from a terrain map, the most usable seems to be `heremaps tin-terrain`_.

.. heremaps tin-terrain: https://github.com/heremaps/tin-terrain


Block world
-----------
The TIN terrain is wonderfully efficient and allows us to build a nice world with amazing scenery and trees, buildings. These trees and buildings must be free-standing. Showing more detail is very difficult, we'd have to carve roads, rivers and settlements in the map.

TODO: Explain concept of grid with Kenney tiles


Web Mercator explained
----------------------

http://www.maptiler.com/google-maps-coordinates-tile-bounds-projection/

Elevation data
--------------


https://docs.mapbox.com/help/troubleshooting/access-elevation-data/
http://viewfinderpanoramas.org/


Land use data
-------------
To "fill in" our world and not just show terrain, we have to know where to place our details. For that we need land use data.

One such source is `osm landuse`_, although it is not directly apparent how one would extract the landcover data, the rendered images would have to be interpreted. In this `paper`_ they have a GeoTIFF file with land cover data of Europe.

TODO: Resolution? Comparison?

Mapbox also has some limited landcover data in their `vector terrain tiles`_, with categories wood/scrub/grass/crop/snow or empty (water or buildings).

.. _osm landuse: https://osmlanduse.org/
.. _paper: https://www.tandfonline.com/doi/full/10.1080/17538947.2018.1550119
.. _vector terrain tiles: https://docs.mapbox.com/vector-tiles/reference/mapbox-terrain-v2/#landcover

Colors
------
We want to use representative colors for our terrain mesh part. There are a few ways to do this:
- Colors with height
- Land use
- Satellite imagery


References
----------
.. [CompGeom] de Berg, Cheong, van Kreveld, Overmars (2008), Computational Geometry
