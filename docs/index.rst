.. xc-game documentation master file, created by
   sphinx-quickstart on Sun Mar  3 16:49:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xc-game's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   terrain


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
