# xc-viz

Terrain 

```
python backend                                  browser
 | <----- /terrain?lat_min=5.12&lon_max=5.67& ---- |
 |                                                 |
 | ------ TIN binary points

```

Backend API routes and arguments.

| Route | Required arguments | Optional args | Returns |
|-------|-----------|-------|----|
| /terrain/tin | x, y (tile coordinates) |  | TIN points in order of importance |
| /terrain/colors/heightmap | name | | Colors for each TIN corner according to heightmap with name *name* |
| /terrain/colors/sat |  |  | Colors for each TIN corner according to satellite imagery (in same order as TIN points are returned) |
| /sky/sat | x, y (tile coordinates), timestamp (rounded to 5mins or other config returned by /sky) |  | Bitmap with cloud cover, from geostationary satellite |

World is divided into tiles without zoom level, they are all the same size (.5 degs?). User can request different percentage of max number of points (relative to 30m grid points) for TIN.

Library is divided into modules:
- Renderer
- Terrain, Sky, Task, PilotPlayback or LivePlayback or GamePlay objects

The Terrain/Sky/Task/Paraglider objects are instantiated and passed to the World object. The World object is given to the Renderer. Before every frame, all objects are updated. The Renderer then reflects these changes.

Any control is done on the child objects. There is a ViewControllerMobile class, ViewControllerDesktop. They are given the Renderer object and control its viewpoint.

All modules have a clear API using a vuejs-like observer pattern.

UI elements like a leaderboard can rank Paraglider objects in the PilotPlayback object using the Task object. They can use Vuejs-style data binding, where data only flows down and events only flow up.
 A File load interface can use a loader like IGCLoader to add tracks to the PilotPlayback object.
 
 The Renderer contains pool of polygons for terrain. It will move any polygons that went out of focus to a part of the terrain that is in focus, with some hysteresis. Might have to think of some smart geometric algorithm to achieve this efficiently, with a TIN we don't know location of triangle perfectly, only with regular grid. But we can use shortcuts like two sorted lists of x, y centroids (or just one corner). It loads TIN point tiles when necessary (for next area), possibly preloading for entire extent.


```ecmascript 6

```

# xc-game

TODO: Update readme, it's outdated.

Hey there, welcome to my pet project!

This readme describes the idea of the game and gives some instructions on playing and developing. Description of algorithms is in the code docs, accessible with `npm run serve:docs` (I'll put them online at some point too).

## Description
*xc-game* is a semi-realistic cross country and hike-and-fly paragliding game. It's designed to be in the right spot on the realistism-fun scale. In other words, I spend a lot of effort on gameplay to make it as much fun as possible. A big part of that is giving the player full control but still doing enough things . Also, coming up with realistic weather algorithms is an interesting challenge, as simulating an entire boundary layer is computationally infeasible and confusing for the user ("is this a thermal or just turbulence?"). So I spend a lot of time trying to figure out how to represent weather phenomena like thermals and wind, how they should behave, what parameters should be adjustable, and how to compute them in a computationally feasible way. So there are dozens of interesting challenges in this game, and then I haven't even started on the heuristics for AI players.

## Play it!
Install `node` and `npm`. Then:

- Install dependencies with `npm install`
- Build with `npm run build`
- Start a simple auto-refreshing http file server at `localhost:8080` with `npm run serve` and pick an example to choose from. At some point I'll put it on a website

The NPM commandline scripts in `package.json` are used to automate everything. No Grunt or Gulp. Keep it simple.

## Folder structure
- `./examples` contains examples used for testing rendering, algorithms etc.
- `./obj` contains all scenery and paraglider models
- `./terrainmaker` is the utility used to generate TINs (Triangular Irregular Network) from DEMs (Digital Elevation Model, grid of heights), giving the game its low-poly style
- `./src` contains the source ES2015 code that gets transpiled with babel
- `./build` Holds compiled js bundle and html, can be copied to server

## Development
Check out the docs with `npm run serve:docs`.

The game loop must run at least 30 times a second, hence the importance of keeping everything predictable. Golden rules:

- No object creation except at initialization. We don't want garbage collection to cause lag. If a function needs e.g. a `THREE.Vector3`, it should be cached for re-use.
- Events (like keyboard/mouse inputs) get stored (e.g. in a `KeyMap`) and only processed on the next game loop run. No other events and asynchronous methods are used (except promises for http requests).
- Config: some values that will probably never get changed can be hardcoded, others should end up in `BASICCONFIG` where they can be overridden. They should have sane default values. They should be namespaced by class name, and nested if the object is instantiated by another class.
- Only touch DOM when really needed to avoid slowdowns. If needed, let a function or class cache state (kind of like virtual DOM).
- As few dependencies as possible, to keep code size as small as possible.

## Reflections on architecture
The classes should not know too much about each other, to avoid problems when changing things. Ideally their interactions are clearly defined in terms of e.g. interfaces. Efficiency is often a factor though, because we must only instantiate objects once.
So let's not init clouds in the Engine constructor, but instead define a method called initClouds, that instantiates cloud meshes. Then another method takes care of updating.

Let's agree on this:
- No class--except the Game class--instantiates objects.
- Initialization is done using `.initThing` methods, updating using `.updateThing` methods.
- Every object that needs it has a `.meta` attribute where external methods can keep state info and a `.cache` attribute for internal state-keeping.
